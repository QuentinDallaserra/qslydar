import QSlydar from './QSlydar';

document.addEventListener('DOMContentLoaded', () => {

    const $usageExampleSlider = document.querySelector('.usage-example-slider');

    if (!$usageExampleSlider) {
        return;
    }

    new QSlydar($usageExampleSlider, {
        prevArrow: true,
        nextArrow: true,
        pagination: true,
        paginationPosition: 'above',
        counter: true,
        counterPosition: 'above',
        variableHeight: true,
        touchScroll: true,
        keyboardNavigation: true,
        autoPlay: true,
        autoPlaySpeed: 4000,
        pauseOnHover: true
    });

}, false);
