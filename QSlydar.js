/*
 ________   ________  ___           ___    ___ ________  ________  ________            ___  ________
|\   __  \ |\   ____\|\  \         |\  \  /  /|\   ___ \|\   __  \|\   __  \          |\  \|\   ____\
\ \  \|\  \\ \  \___|\ \  \        \ \  \/  / | \  \_|\ \ \  \|\  \ \  \|\  \         \ \  \ \  \___|_
 \ \  \\\  \\ \_____  \ \  \        \ \    / / \ \  \ \\ \ \   __  \ \   _  _\      __ \ \  \ \_____  \
  \ \  \\\  \\|____|\  \ \  \____    \/  /  /   \ \  \_\\ \ \  \ \  \ \  \\  \| ___|\  \\_\  \|____|\  \
   \ \_____  \ ____\_\  \ \_______\__/  / /      \ \_______\ \__\ \__\ \__\\ _\|\__\ \________\____\_\  \
    \|___| \__\\_________\|_______|\___/ /        \|_______|\|__|\|__|\|__|\|__\|__|\|________|\_________\
          \|__\|_________|        \|___|/                                                     \|_________|


    Version: 0.6.0
     Author: Quentin Dallaserra
    Website: http://Quentin.XYZ
       Repo: https://bitbucket.org/QuentinDallaserra/qslydar


   Settings:        Setting             Type          Default        Options
              -------------------------------------------------------------------
              prevArrow           |    boolean    |    true    |    true/false
              -------------------------------------------------------------------
              nextArrow           |    boolean    |    true    |    true/false
              -------------------------------------------------------------------
              pagination          |    boolean    |    false   |    true/false
              -------------------------------------------------------------------
              paginationPosition  |    string     |    below   |    above/below
              -------------------------------------------------------------------
              counter             |    boolean    |    false   |    true/false
              -------------------------------------------------------------------
              counterPosition     |    string     |    above   |    above/below
              -------------------------------------------------------------------
              variableHeight      |    boolean    |    true    |    true/false
              -------------------------------------------------------------------
              centerMode          |    boolean    |    false   |    true/false
              -------------------------------------------------------------------
              centerModePaddingX  |    number     |     100    |    unlimited
              -------------------------------------------------------------------
              centerModePaddingY  |    number     |      0     |    unlimited
              -------------------------------------------------------------------
              touchScroll         |    boolean    |    true    |    true/false
              -------------------------------------------------------------------
              keyboardNavigation  |    boolean    |    true    |    true/false
              -------------------------------------------------------------------
              pauseOnHover        |    boolean    |    false   |    true/false
              -------------------------------------------------------------------
              autoPlay            |    boolean    |    false   |    true/false
              -------------------------------------------------------------------
              autoPlaySpeed       |    number     |    4000    |    unlimited
              -------------------------------------------------------------------
              animation           |    string     |    slide   |    slide/fade/scale
              -------------------------------------------------------------------
              scaleAmount         |    number     |     80     |    100
              -------------------------------------------------------------------
              animationDuration   |    number     |     300    |    unlimited
              -------------------------------------------------------------------
*/

export default function QSlydar(element, settings) {

  if (element === null) {

    throw new SyntaxError('You forgot to specify an element!');

  }

  const qslydar = this;

  qslydar.element = element;
  qslydar.settings = settings;
  qslydar.slides = Array.from(element.children);
  qslydar.slideCount = qslydar.slides.length;
  qslydar.touchstartX = 0;
  qslydar.touchendX = 0;
  qslydar.currentSlide = 0;
  qslydar.interval = 0;
  qslydar.paginationButtons;
  qslydar.slideTrack;
  qslydar.slideContainer;
  qslydar.count;

  qslydar.defaultSettings = {
    prevArrow: true,
    nextArrow: true,
    pagination: false,
    paginationPosition: 'below',
    counter: false,
    counterPosition: 'above',
    variableHeight: true,
    centerMode: false,
    centerModePaddingX: 100,
    centerModePaddingY: 0,
    touchScroll: true,
    keyboardNavigation: true,
    autoPlay: false,
    autoPlaySpeed: 4000,
    pauseOnHover: false,
    animation: 'slide',
    scaleAmount: 80,
    animationDuration: 300,
  };

  qslydar.options = {
    ...qslydar.defaultSettings,
    ...qslydar.settings,
  };

  qslydar.initialize();

}

QSlydar.prototype.initialize = function () {

  const qslydar = this;

  qslydar.createSlideContainer();
  qslydar.createButtons();
  qslydar.createPagination();
  qslydar.createCounter();
  qslydar.createSlides();
  qslydar.resizeSlides();
  qslydar.resizeSlideTrack();
  qslydar.resizeSlideContainer();
  qslydar.positionSlides();
  qslydar.prepareAnimationEffect();
  qslydar.enableTouchScroll();
  qslydar.enableKeyboardNavigation();
  qslydar.autoPlay();
  qslydar.pauseOnHover();
  qslydar.windowResizeHandler();

};

QSlydar.prototype.settingEnabled = function (...option) {

  const qslydar = this;

  return qslydar.slideCount > 1 && option.every(option => option === true);

};

QSlydar.prototype.centerModePadding = function () {

  const qslydar = this;

  return {
    centerModePaddingX:
      qslydar.settingEnabled(qslydar.options.centerMode) && qslydar.options.centerModePaddingX,
    centerModePaddingY:
      qslydar.settingEnabled(qslydar.options.centerMode) && qslydar.options.centerModePaddingY,
  };

};

QSlydar.prototype.createSlideContainer = function () {

  const qslydar = this;

  qslydar.element.className += ' q-slydar';

  const containerNode = document.createElement('div');
  containerNode.className = 'q-slydar__container';

  qslydar.element.insertBefore(containerNode, qslydar.element.firstChild);

  const trackNode = document.createElement('div');
  trackNode.className = 'q-slydar__track';

  containerNode.insertBefore(trackNode, containerNode.firstChild);

  qslydar.slideTrack = qslydar.element.querySelector('.q-slydar__track');
  qslydar.slideContainer = qslydar.element.querySelector('.q-slydar__container');

};

QSlydar.prototype.createSlides = function () {

  const qslydar = this;

  qslydar.slides.forEach((item, index) => {

    const currentClass = index === 0 ? ' q-slydar__slide active' : ' q-slydar__slide';

    item.className += currentClass;
    qslydar.slideTrack.appendChild(item);

  });

};

QSlydar.prototype.resizeSlides = function () {

  const qslydar = this;

  const containerWidth = qslydar.slideContainer.offsetWidth;

  qslydar.slides.forEach(item => {

    item.style.width = `${containerWidth - qslydar.centerModePadding().centerModePaddingX}px`;

  });

};

QSlydar.prototype.positionSlides = function () {

  const qslydar = this;

  let itemPosition = 0;

  qslydar.slides.forEach(item => {

    const centerPaddingMode = qslydar.options.animation === 'scale'
      ? 0
      : qslydar.centerModePadding().centerModePaddingX / 4;

    const itemWidth = item.offsetWidth + centerPaddingMode;

    item.style.left = `${itemPosition}px`;

    itemPosition += itemWidth;

  });

};

QSlydar.prototype.resizeSlideTrack = function () {

  const qslydar = this;

  let trackWidth = 0;

  qslydar.slides.forEach(item => {

    const itemWidth = item.offsetWidth;
    trackWidth += itemWidth;

  });

  qslydar.slideTrack.style.width = `${trackWidth}px`;

};

QSlydar.prototype.resizeSlideContainer = function () {

  const qslydar = this;

  const slideHeight = qslydar.slides[qslydar.currentSlide].offsetHeight
    + qslydar.centerModePadding().centerModePaddingY;
  let maxHeightSlide = 0;

  if (qslydar.settingEnabled(qslydar.options.variableHeight)) {

    qslydar.slideContainer.style.height = `${slideHeight}px`;

  } else {

    qslydar.slides.forEach(item => {

      const itemHeight = item.offsetHeight;

      itemHeight > maxHeightSlide
        ? (maxHeightSlide = itemHeight)
        : (maxHeightSlide = maxHeightSlide);

    });

    qslydar.slideContainer.style.height = `${maxHeightSlide}px`;

  }

};

QSlydar.prototype.createButtons = function () {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.prevArrow)) {

    qslydar.slideContainer.insertAdjacentHTML(
      'beforeend',
      '<button class="q-slydar__arrow q-slydar__arrow--prev">Previous</button>',
    );
    qslydar.slideContainer
      .querySelector('.q-slydar__arrow--prev')
      .addEventListener('click', qslydar.increment.bind(qslydar, -1));

  }

  if (qslydar.settingEnabled(qslydar.options.nextArrow)) {

    qslydar.slideContainer.insertAdjacentHTML(
      'beforeend',
      '<button class="q-slydar__arrow q-slydar__arrow--next">Next</button>',
    );
    qslydar.slideContainer
      .querySelector('.q-slydar__arrow--next')
      .addEventListener('click', qslydar.increment.bind(qslydar, 1));

  }

};

QSlydar.prototype.createCounter = function () {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.counter)) {

    const counterNode = document.createElement('div');
    counterNode.className = 'q-slydar__counter';

    const counterPosition = qslydar.options.counterPosition === 'below'
      ? qslydar.slideContainer.nextSibling
      : qslydar.slideContainer;

    qslydar.slideContainer.parentNode.insertBefore(counterNode, counterPosition);

    counterNode.innerHTML += `<span class="q-slydar__count">${
      qslydar.currentSlide
    }</span> / <span class="q-slydar__total">${qslydar.slideCount}</span>`;

    qslydar.count = qslydar.element.querySelector('.q-slydar__count');

  }

};

QSlydar.prototype.updateCounter = function () {

  const qslydar = this;

  qslydar.count.innerHTML = qslydar.currentSlide;

};

QSlydar.prototype.createPagination = function () {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.pagination)) {

    const paginationNode = document.createElement('ul');
    paginationNode.className = 'q-slydar__pagination';

    const paginationPosition = qslydar.options.paginationPosition === 'below'
      ? qslydar.slideContainer.nextSibling
      : qslydar.slideContainer;

    qslydar.slideContainer.parentNode.insertBefore(paginationNode, paginationPosition);

    qslydar.slides.forEach((item, index) => {

      paginationNode.innerHTML += `<li class=${index === 0 ? 'active' : ''}><button>${index
        + 1}</button></li>`;

    });

    qslydar.goToPaginationIndex();

  }

};

QSlydar.prototype.goToPaginationIndex = function () {

  const qslydar = this;

  qslydar.paginationButtons = Array.from(
    qslydar.element.parentNode.querySelector('.q-slydar__pagination').children,
  );

  qslydar.paginationButtons.forEach((item, index) => {

    item.addEventListener('click', () => {

      qslydar.currentSlide = index;
      qslydar.slideTo();

    });

  });

};

QSlydar.prototype.increment = function (index) {

  const qslydar = this;

  qslydar.currentSlide = (qslydar.currentSlide + (qslydar.slideCount + index)) % qslydar.slideCount;
  qslydar.slideTo();

};

QSlydar.prototype.prepareAnimationEffect = function () {

  const qslydar = this;
  const slidePosition = -(
    qslydar.slides[qslydar.currentSlide].offsetLeft
    - qslydar.centerModePadding().centerModePaddingX / 2
  );

  qslydar.slideTrack.style.transform = `translate3d(${slidePosition}px, 0 , 0)`;

  if (qslydar.options.animation === 'scale') {

    const scaleAmount = qslydar.options.scaleAmount;

    qslydar.slides.map(item => {

      item.style.transform = `scale(${scaleAmount / 100})`;

    });

    qslydar.slides[qslydar.currentSlide].style.transform = 'scale(1)';

  }

};

QSlydar.prototype.scaleEffect = function () {

  const qslydar = this;
  const duration = qslydar.options.animationDuration;

  qslydar.prepareAnimationEffect();

  qslydar.slides.map(item => {

    item.style.transition = `all ease-in-out ${duration / 1000}s`;

  });

  qslydar.slideTrack.style.transition = `all ease-in-out ${duration / 1000}s`;
  qslydar.slideContainer.style.transition = `all ease-in-out ${duration / 1000}s`;

};

QSlydar.prototype.slideEffect = function () {

  const qslydar = this;
  const duration = qslydar.options.animationDuration;

  qslydar.prepareAnimationEffect();

  qslydar.slideTrack.style.transition = `all ease-in-out ${duration / 1000}s`;
  qslydar.slideContainer.style.transition = `all ease-in-out ${duration / 1000}s`;

};

QSlydar.prototype.fadeEffect = function () {

  const qslydar = this;
  const duration = qslydar.options.animationDuration;

  qslydar.slides.forEach(item => {

    item.style.opacity = '0';
    item.style.transition = `opacity ease-in-out ${duration / 1000}s`;

  });

  qslydar.slideContainer.style.transition = `all ease-in-out ${(duration / 1000) * 2}s`;

  setTimeout(() => {

    qslydar.prepareAnimationEffect();
    qslydar.slides[qslydar.currentSlide].style.opacity = '1';

  }, duration);

};

QSlydar.prototype.sliderAnimation = function () {

  const qslydar = this;
  const animationOption = qslydar.options.animation;

  switch (animationOption) {

    case 'slide':
      qslydar.slideEffect();
      break;
    case 'fade':
      qslydar.fadeEffect();
      break;
    case 'scale':
      qslydar.scaleEffect();
      break;
    default:
      return;

  }

  qslydar.resizeSlideContainer();

};

QSlydar.prototype.slideTo = function () {

  const qslydar = this;

  qslydar.slides.forEach(item => {

    item.classList.remove('active');

  });

  qslydar.slides[qslydar.currentSlide].classList.add('active');

  if (qslydar.settingEnabled(qslydar.options.pagination)) {

    qslydar.paginationButtons.forEach(item => {

      item.classList.remove('active');

    });

    qslydar.paginationButtons[qslydar.currentSlide].classList.add('active');

  }

  qslydar.sliderAnimation();

  if (qslydar.settingEnabled(qslydar.options.autoPlay)) {

    qslydar.stopPlay();
    qslydar.autoPlay();

  }

  if (qslydar.settingEnabled(qslydar.options.counter)) {

    qslydar.updateCounter();

  }

};

QSlydar.prototype.autoPlay = function () {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.autoPlay)) {

    qslydar.interval = setInterval(() => {

      qslydar.increment(1);

    }, qslydar.options.autoPlaySpeed);

  }

};

QSlydar.prototype.stopPlay = function () {

  const qslydar = this;

  clearInterval(qslydar.interval);

};

QSlydar.prototype.pauseOnHover = function () {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.autoPlay, qslydar.options.pauseOnHover)) {

    qslydar.element.addEventListener('mouseover', () => {

      clearInterval(qslydar.interval);

    });

    qslydar.element.addEventListener('mouseleave', () => {

      qslydar.stopPlay();
      qslydar.autoPlay();

    });

  }

};

QSlydar.prototype.handleGesture = function () {

  const qslydar = this;

  if (qslydar.touchendX <= qslydar.touchstartX) {

    qslydar.increment(1);

  }

  if (qslydar.touchendX >= qslydar.touchstartX) {

    qslydar.increment(-1);

  }

};

QSlydar.prototype.enableTouchScroll = function () {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.touchScroll)) {

    qslydar.element.addEventListener(
      'touchstart',
      event => {

        qslydar.touchstartX = event.changedTouches[0].screenX;

      },
      false,
    );

    qslydar.element.addEventListener(
      'touchend',
      event => {

        qslydar.touchendX = event.changedTouches[0].screenX;
        qslydar.handleGesture();

      },
      false,
    );

  }

};

QSlydar.prototype.enableKeyboardNavigation = function (event) {

  const qslydar = this;

  if (qslydar.settingEnabled(qslydar.options.keyboardNavigation)) {

    document.onkeydown = function (event) {

      event = event || window.event;

      switch (event.which || event.keyCode) {

        case 37:
          qslydar.increment(-1);
          break;
        case 39:
          qslydar.increment(1);
          break;
        default:
          return;

      }

      event.preventDefault();

    };

  }

};

QSlydar.prototype.windowResizeHandler = function () {

  const qslydar = this;

  window.addEventListener(
    'resize',
    () => {

      qslydar.resizeSlides();
      qslydar.resizeSlideTrack();
      qslydar.resizeSlideContainer();
      qslydar.positionSlides();
      qslydar.prepareAnimationEffect();

    },
    true,
  );

};
